using Furion.DynamicApiController;
using System;
using System.Globalization;

namespace DateTimeDemo.Application
{
    /// <summary>
    /// 系统服务接口
    /// </summary>
    public class SystemAppService : IDynamicApiController
    {
        private readonly ISystemService _systemService;
        public SystemAppService(ISystemService systemService)
        {
            _systemService = systemService;
        }

        /// <summary>
        /// 获取系统描述
        /// </summary>
        /// <returns></returns>
        public string GetDescription()
        {
            return _systemService.GetDescription();
        }

        /// <summary>
        /// 获取系统当前时间
        /// </summary>
        /// <returns></returns>
        public string GetTimeNow()
        {
            //return DateTime.Now.ToString(); //  6/9/2564 11:34:04
            //return DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"); // 2564-09-06 11:34:04
            return DateTime.Now.ToString(CultureInfo.InvariantCulture.DateTimeFormat.SortableDateTimePattern, CultureInfo.InvariantCulture); //2021-11-21T17:01:47
        }
    }
}
