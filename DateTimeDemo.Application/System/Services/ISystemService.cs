﻿namespace DateTimeDemo.Application
{
    public interface ISystemService
    {
        string GetDescription();
    }
}