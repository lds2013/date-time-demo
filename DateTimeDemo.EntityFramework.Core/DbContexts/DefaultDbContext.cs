﻿using Furion.DatabaseAccessor;
using Microsoft.EntityFrameworkCore;

namespace DateTimeDemo.EntityFramework.Core
{
    [AppDbContext("DateTimeDemo", DbProvider.Sqlite)]
    public class DefaultDbContext : AppDbContext<DefaultDbContext>
    {
        public DefaultDbContext(DbContextOptions<DefaultDbContext> options) : base(options)
        {
        }
    }
}