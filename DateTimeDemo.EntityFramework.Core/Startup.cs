﻿using Furion;
using Microsoft.Extensions.DependencyInjection;

namespace DateTimeDemo.EntityFramework.Core
{
    public class Startup : AppStartup
    {
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDatabaseAccessor(options =>
            {
                options.AddDbPool<DefaultDbContext>();
            }, "DateTimeDemo.Database.Migrations");
        }
    }
}